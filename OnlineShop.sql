﻿-- Create tables
CREATE TABLE dbo.[user] (
    email VARCHAR(60) PRIMARY KEY,
    name VARCHAR(30),
    mobile BIGINT,
    address VARCHAR(250),
    pincode INT,
    password VARCHAR(20)
);

CREATE TABLE dbo.product (
    pid VARCHAR(45) PRIMARY KEY,
    pname VARCHAR(100),
    ptype VARCHAR(20),
    pinfo VARCHAR(350),
    pprice DECIMAL(12,2),
    pquantity INT,
    image VARBINARY(MAX)
);


CREATE TABLE dbo.orders (
    orderid VARCHAR(45) PRIMARY KEY, 
    prodid VARCHAR(45),
    quantity INT,
    amount DECIMAL(10,2),
    shipped INT DEFAULT 0,
    FOREIGN KEY (prodid) REFERENCES dbo.product(pid) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE dbo.transactions (
    transid VARCHAR(45) PRIMARY KEY,
    username VARCHAR(60),
    [time] DATETIME,
    amount DECIMAL(10,2),
    FOREIGN KEY (transid) REFERENCES dbo.orders(orderid) ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (username) REFERENCES dbo.[user](email) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE dbo.setting (
    setting_id INT PRIMARY KEY,
    type_id INT,
    title VARCHAR(50) NOT NULL,
    details VARCHAR(100),
    priority INT,
    is_active BIT,
    FOREIGN KEY (type_id) REFERENCES dbo.setting(setting_id) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE dbo.user_role (
    email VARCHAR(60),
    role_id INT,
    is_active BIT,
    PRIMARY KEY (email, role_id),
    FOREIGN KEY (email) REFERENCES dbo.[user](email) ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (role_id) REFERENCES dbo.setting(setting_id) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE dbo.user_demand (
    username VARCHAR(60),
    prodid VARCHAR(45),
    quantity INT,
    PRIMARY KEY (username, prodid),
    FOREIGN KEY (prodid) REFERENCES dbo.product(pid) ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (username) REFERENCES dbo.[user](email) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE dbo.usercart (
    username VARCHAR(60),
    prodid VARCHAR(45),
    quantity INT,
    PRIMARY KEY (username, prodid),
    FOREIGN KEY (prodid) REFERENCES dbo.product(pid) ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (username) REFERENCES dbo.[user](email) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- Chèn dữ liệu giả vào bảng user
INSERT INTO dbo.[user] (email, name, mobile, address, pincode, password)
VALUES
    ('user1@gmail.com', 'User 1', 123456789, 'Address 1', 12345, 'password1'),
    ('user2@gmail.com', 'User 2', 987654321, 'Address 2', 54321, 'password2'),
    ('user3@gmail.com', 'User 3', 111222333, 'Address 3', 67890, 'password3'),
    ('user4@gmail.com', 'User 4', 999888777, 'Address 4', 98765, 'password4'),
    ('user5@gmail.com', 'User 5', 555444333, 'Address 5', 54321, 'password5');

-- Chèn dữ liệu giả vào bảng product
INSERT INTO dbo.product (pid, pname, ptype, pinfo, pprice, pquantity, image)
VALUES
    ('P20230423082243', 'Product 1', 'Type 1', 'Info 1', 100.00, 50, NULL),
    ('P20230423083830', 'Product 2', 'Type 2', 'Info 2', 200.00, 30, NULL),
    ('P20230423084143', 'Product 3', 'Type 3', 'Info 3', 150.00, 20, NULL),
    ('P20230423084144', 'Product 4', 'Type 1', 'Info 4', 120.00, 40, NULL),
    ('P20230423084145', 'Product 5', 'Type 2', 'Info 5', 180.00, 60, NULL);

-- Chèn dữ liệu giả vào bảng orders
INSERT INTO dbo.orders (orderid, prodid, quantity, amount, shipped)
VALUES
    ('Order1', 'P20230423082243', 2, 200.00, 1),
    ('Order2', 'P20230423083830', 1, 100.00, 0),
    ('Order3', 'P20230423084143', 3, 450.00, 1),
    ('Order4', 'P20230423084144', 1, 120.00, 1),
    ('Order5', 'P20230423084145', 2, 360.00, 0);

-- Chèn dữ liệu giả vào bảng transactions
INSERT INTO dbo.transactions (transid, username, time, amount)
VALUES
    ('Transaction1', 'user1@gmail.com', GETDATE(), 300.00),
    ('Transaction2', 'user2@gmail.com', GETDATE(), 150.00),
    ('Transaction3', 'user3@gmail.com', GETDATE(), 200.00),
    ('Transaction4', 'user4@gmail.com', GETDATE(), 180.00),
    ('Transaction5', 'user5@gmail.com', GETDATE(), 400.00);

-- Chèn dữ liệu giả vào bảng setting
INSERT INTO dbo.setting (setting_id, type_id, title, details, priority, is_active)
VALUES
    (1, NULL, 'Setting 1', 'Details 1', 1, 1),
    (2, NULL, 'Setting 2', 'Details 2', 2, 1),
    (3, NULL, 'Setting 3', 'Details 3', 3, 1),
    (4, NULL, 'Setting 4', 'Details 4', 4, 1),
    (5, NULL, 'Setting 5', 'Details 5', 5, 1);

-- Chèn dữ liệu giả vào bảng user_role
INSERT INTO dbo.user_role (email, role_id, is_active)
VALUES
    ('user1@gmail.com', 1, 1),
    ('user2@gmail.com', 2, 1),
    ('user3@gmail.com', 3, 1),
    ('user4@gmail.com', 4, 1),
    ('user5@gmail.com', 5, 1);

-- Chèn dữ liệu giả vào bảng user_demand
INSERT INTO dbo.user_demand (username, prodid, quantity)
VALUES
    ('user1@gmail.com', 'P20230423082243', 3),
    ('user2@gmail.com', 'P20230423083830', 1),
    ('user3@gmail.com', 'P20230423084143', 2),
    ('user4@gmail.com', 'P20230423084144', 1),
    ('user5@gmail.com', 'P20230423084145', 2);

-- Chèn dữ liệu giả vào bảng usercart
INSERT INTO dbo.usercart (username, prodid, quantity)
VALUES
    ('user1@gmail.com', 'P20230423082243', 1),
    ('user2@gmail.com', 'P20230423083830', 2),
    ('user3@gmail.com', 'P20230423084143', 1),
    ('user4@gmail.com', 'P20230423084144', 3),
    ('user5@gmail.com', 'P20230423084145', 2);

CREATE TABLE categories (
    category_id INT PRIMARY KEY,
    category_name VARCHAR(100) NOT NULL
);
ALTER TABLE product 
ADD category_id INT;
ALTER TABLE product 
ADD FOREIGN KEY (category_id) REFERENCES categories(category_id);

INSERT INTO categories (category_id, category_name) VALUES
    (1, 'Category 1'),
    (2, 'Category 2'),
    (3, 'Category 3'),
    (4, 'Category 4'),
    (5, 'Category 5');