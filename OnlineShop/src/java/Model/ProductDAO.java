/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import dal.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ProductDAO extends DBContext {

    //khai bao cac thanh phan xu ly
    Connection cnn;//ket noi DB
    PreparedStatement stm;//thuc hien cac cau lenh sql
    ResultSet rs;//luu tru va xu ly du lieu dc lay ve tu client

    public ProductDAO() {
        connection();
    }

    public void connection() {
        try {
            cnn = connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail");
            }
        } catch (Exception e) {
            System.out.println("Connect error:" + e.getMessage());
        }
    }

    public List<Product> getAllProduct() throws SQLException {
        List<Product> list = new ArrayList<>();
        String query = "select * from product";
        try {
            stm = cnn.prepareStatement(query);
            rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2), 
                        rs.getInt(3),
                        rs.getString(4), 
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7), 
                        rs.getString(8)));
            }
        } catch (Exception e) {
            System.out.println("getAllProduct:" + e.getMessage());
        }
        return list;
    }
    
    public List<Categories> getAllCategory() throws SQLException {
        List<Categories> list = new ArrayList<>();
        String query = "select * from categories";
        try {
            stm = cnn.prepareStatement(query);
            rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Categories(rs.getInt(1),
                        rs.getString(2)));
            }
        } catch (Exception e) {
            System.out.println("getAllCategory:" + e.getMessage());
        }
        return list;
    }

    public static void main(String[] args) throws SQLException {
        ProductDAO dao = new ProductDAO();
        List<Product> list = dao.getAllProduct();
        List<Categories> listC = dao.getAllCategory();
        for (Categories o : listC) {
            System.out.println(o.getCategory_name());
        }
    }
}
