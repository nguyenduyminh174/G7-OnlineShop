--Create database onlineshop_demo

CREATE TABLE dbo.[role] (
    roleid INT PRIMARY KEY identity(1,1),
    rolename VARCHAR(50),
 --   FOREIGN KEY (role_id) REFERENCES dbo.setting(setting_id) 
);

-- Create tables
CREATE TABLE dbo.[user] (
	id INT PRIMARY KEY identity(1,1),
	roleid INT,
    email VARCHAR(60),
    name VARCHAR(30),
    mobile BIGINT,
    address VARCHAR(250),
    pincode INT,
    password VARCHAR(20)
	FOREIGN KEY (roleid) REFERENCES dbo.[role](roleid)
);

CREATE TABLE categories (
    category_id INT PRIMARY KEY identity(1,1),
    category_name VARCHAR(100) NOT NULL
);

CREATE TABLE dbo.product (
    pid INT PRIMARY KEY identity(1,1),
    pname VARCHAR(100),
	Category_id INT,
    ptype VARCHAR(20),
    pinfo VARCHAR(350),
    pprice DECIMAL(12,2),
    pstock INT,
    image VARBINARY(MAX),
	FOREIGN KEY (category_id) REFERENCES categories(category_id)
);

CREATE TABLE dbo.orders (
    orderid INT PRIMARY KEY identity(1,1),
	userid INT,
    [time] DATETIME,
    total_price DECIMAL(10,2),
    FOREIGN KEY (userid) REFERENCES dbo.[user](id)
);

CREATE TABLE dbo.order_detail (
	id INT PRIMARY KEY identity(1,1),
    orderid INT , 
    prodid INT,
    quantity INT,
    price DECIMAL(10,2),
    shipped INT DEFAULT 0,
    FOREIGN KEY (prodid) REFERENCES dbo.product(pid) ,
	FOREIGN KEY (orderid) REFERENCES dbo.orders(orderid) 
);


CREATE TABLE dbo.setting (
    setting_id INT PRIMARY KEY identity(1,1),
    type_id INT,
    title VARCHAR(50) NOT NULL,
    details VARCHAR(100),
    priority INT,
    is_active BIT,
    FOREIGN KEY (type_id) REFERENCES dbo.setting(setting_id) 
);


CREATE TABLE dbo.user_demand (
    userid INT,
    prodid INT,
    quantity INT,
    PRIMARY KEY (userid, prodid),
    FOREIGN KEY (prodid) REFERENCES dbo.product(pid) ,
    FOREIGN KEY (userid) REFERENCES dbo.[user](id) 
);

CREATE TABLE dbo.usercart (
    userid INT,
    prodid INT,
    quantity INT,
    PRIMARY KEY (userid, prodid),
    FOREIGN KEY (prodid) REFERENCES dbo.product(pid) ,
    FOREIGN KEY (userid) REFERENCES dbo.[user](id) 
);

INSERT INTO dbo.[role] (rolename)
VALUES 
    ('Admin'),
    ('Customer');

INSERT INTO dbo.[user] (roleid, email, name, mobile, address, pincode, password)
VALUES 
    (1, 'admin@example.com', 'Admin User', 1234567890, '123 Admin St', 12345, 'adminpass'),
    (2, 'manager@example.com', 'Customer', 9876543210, '456 Manager St', 54321, 'managerpass'),
    (2, 'customer1@example.com', 'Customer One', 1122334455, '101 Customer St', 54321, 'customer1pass'),
    (2, 'customer2@example.com', 'Customer Two', 9988776655, '202 Customer St', 67890, 'customer2pass');

INSERT INTO categories (category_name)
VALUES 
    ('Smart Phone'),
    ('LapTop'),
    ('Camera'),
    ('TVs'),
    ('Smart Watch');

INSERT INTO dbo.product (pname, Category_id, ptype, pinfo, pprice, pstock, image)
VALUES 
    ('Smart Phone', 1, 'Smart Phone', 'High-performance laptop', 999.99, 50, 0x),
    ('LapTop', 2, 'LapTop', 'Cotton T-shirt', 19.99, 100, 0x),
    ('Camera', 3, 'Camera', 'Bestseller book', 12.49, 200, 0x),
    ('TVs', 4, 'TVs', 'Automatic coffee maker', 49.99, 30, 0x),
    ('Smart Watch', 5, 'Smart Watch', 'Remote-controlled toy car', 29.99, 50, 0x);

INSERT INTO dbo.orders (userid, [time], total_price)
VALUES 
    (4, GETDATE(), 79.98),
    (3, GETDATE(), 61.48),
    (4, GETDATE(), 25.99),
    (3, GETDATE(), 35.79),
    (4, GETDATE(), 99.95);

INSERT INTO dbo.order_detail (orderid, prodid, quantity, price, shipped)
VALUES 
    (1, 1, 2, 51.98, 0),
    (2, 3, 3, 46.47, 1),
    (3, 1, 1, 25.99, 0),
    (4, 4, 1, 35.79, 0),
    (5, 2, 3, 32.97, 0);

INSERT INTO dbo.setting (type_id, title, details, priority, is_active)
VALUES 
    (NULL, 'Setting 1', 'Details for Setting 1', 1, 1),
    (NULL, 'Setting 2', 'Details for Setting 2', 2, 1),
    (NULL, 'Setting 3', 'Details for Setting 3', 3, 0),
    (NULL, 'Setting 4', 'Details for Setting 4', 4, 1),
    (NULL, 'Setting 5', 'Details for Setting 5', 5, 0);

INSERT INTO dbo.user_demand (userid, prodid, quantity)
VALUES 
    (4, 2, 2),
    (5, 1, 3),
    (4, 4, 1),
    (5, 3, 2),
    (4, 5, 2);

INSERT INTO dbo.usercart (userid, prodid, quantity)
VALUES 
    (4, 3, 2),
    (5, 4, 1),
    (4, 1, 3),
    (5, 5, 2),
    (4, 2, 1);


